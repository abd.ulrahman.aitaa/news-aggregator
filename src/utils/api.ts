import axios, { AxiosInstance } from 'axios';
import { 
  NEWSAPI_BASE_URL, 
  NEWSAPI_KEY, 
  NewsAPI_EndPoint,
  THEGUARDIANAPI_BASE_URL, 
  THEGUARDIANAPI_KEY,
  THEGUARDIANAPI_EndPoint,
  NEWYORKTIMESAPI_BASE_URL, 
  NEWYORKTIMESAPI_KEY,
  NEWYORKTIMESAPI_EndPoint,
  PAGE_SIZE
} from '../constants/consts';
import { Article } from '../models/Article';

interface ApiConfig {
  baseURL: string;
  headers?: Record<string, string>;
  params?: Record<string, string>;
}

// Factory function to create Axios instances
function createAPIInstance(config: ApiConfig): AxiosInstance {
  return axios.create(config);
}

// Define data sources configuration
const dataSources = {
  newsAPI: {
    baseURL: NEWSAPI_BASE_URL ?? '',
    headers: {
      'X-Api-Key': NEWSAPI_KEY ?? '',
    },
  },
  theGuardianAPI:{
    baseURL: THEGUARDIANAPI_BASE_URL ?? '',
    params: {
      'api-key':THEGUARDIANAPI_KEY ?? '',
    },
  },
  newYorkTimesAPI:{
    baseURL: NEWYORKTIMESAPI_BASE_URL ?? '',
    params: {
      'api-key':NEWYORKTIMESAPI_KEY ?? '',
    },
  }
};

// Create Axios instances for each data source
const newsAPIInstance = createAPIInstance(dataSources.newsAPI);
const theGuardianAPIInstance = createAPIInstance(dataSources.theGuardianAPI);
const newYorkTimesAPIInstance = createAPIInstance(dataSources.newYorkTimesAPI);

// Define functions to make requests to each endpoint
export const fetchNewsFromNewsAPI = async (query: string, page: number, orderBy: string) => {
  try {
    const response = await newsAPIInstance.get(NewsAPI_EndPoint, {
      params: { q: query, 'page': page, 'pageSize' : PAGE_SIZE, 'sortBy': orderBy },
      // params: { q: query, 'page': page, 'pageSize' : PAGE_SIZE, 'sortBy': orderBy, 'category': 'technology' },
      // params: { q: query, 'page': page, 'sortBy': orderBy },
    });

    const res = response.data;
    const totalResults = res.totalResults;

    const articles = res.articles.map((article: Article) => {
      return {
        ...article,
        source: 'NewsAPI.org'
      };
    });

    // return articles;
    return {newsAPIArticles: articles, newsAPITotal: totalResults};
  } catch (error) {
    console.error('Error fetching news from NewsAPI:', error);
    throw error;
  }
};

export const fetchNewsFromTheGuardianAPI = async (query: string, page: number, orderBy: string, showFields?: string, category?: string) => {
  try {
    const response = await theGuardianAPIInstance.get(THEGUARDIANAPI_EndPoint, {
      params: { q: query, 'page': page, 'page-size': PAGE_SIZE, 'order-by': orderBy, 'show-fields': showFields, 'section': category },
      // params: { q: query, 'page': page, 'page-size': PAGE_SIZE, 'order-by': orderBy, 'show-fields': showFields, 'section': 'technology' },
      // params: { q: query, 'page': page, 'order-by': orderBy, 'show-fields': showFields },
    });

    const res = response.data.response;
    const totalResults = res.total;

    const articles = res.results.map((article: any) => ({
      id: article.id,
      title: article.webTitle,
      description: article?.fields?.trailText,
      author: '', // The Guardian API doesn't provide author information in this response
      publishedAt: article.webPublicationDate,
      source: 'The Guardian'
    }));
    
    // return articles;
    return {guardianAPIArticles: articles, guardianAPITotal: totalResults};
  } catch (error) {
    console.error('Error fetching news from TheGuardianAPI:', error);
    throw error;
  }
};

export const fetchNewsFromNewYorkTimesAPI = async (query: string, page: number, orderBy: string, categories?: string[] ) => {
  try {
    let fqValue = '';
    if (categories && categories.length > 0) {
      // Construct the fq parameter value based on the categories array
      fqValue = categories.map((category) => `news_desk:("${category}")`).join(' OR ');
    }

    const response = await newYorkTimesAPIInstance.get(NEWYORKTIMESAPI_EndPoint, {
      params: { q: query, 'page': page, 'sort': orderBy, 'fq': fqValue },
      // params: { q: query, 'page': page, 'sort': orderBy, 'fq': 'news_desk:("Sports", "technology") AND source:("The New York Times")' },
      // params: { q: query, 'page': page, 'sort': orderBy, 'fq': 'news_desk:("sports", "technology")' },
    });

    const res = response.data.response;
    const totalResults =res.meta.hits;

    const articles = res.docs.map((article: any) => ({
      id: article._id,
      title: article.headline.main,
      description: article.lead_paragraph,
      author: article.byline.original, // This may not always be available
      publishedAt: article.pub_date,
      source: 'The New York Times',
    }));
    
    // return articles;
    return {newYorkTimesAPIArticles: articles, newYorkTimesAPITotal: totalResults};
  } catch (error) {
    console.error('Error fetching news from NewYorkTimesAPI:', error);
    throw error;
  }
};
