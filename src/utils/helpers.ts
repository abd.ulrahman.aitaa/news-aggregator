export const formatDate = (date: Date): string => {
  const options: Intl.DateTimeFormatOptions = {
    year: 'numeric',
    month: 'long',
    day: 'numeric'
  };
  return new Intl.DateTimeFormat('en-US', options).format(date);
};

export const formatStringDate = (dateString: string, showTime: boolean = false): string => {
  const options: Intl.DateTimeFormatOptions = {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  };

  if (showTime) {
    options.hour = 'numeric';
    options.minute = 'numeric';
    options.second = 'numeric';
  }

  return new Date(dateString).toLocaleDateString('en-US', options);
};

export const calculateTotalPages = (totalArticles: number, pageSize: number): number => {
  const totalPages = Math.ceil(totalArticles / pageSize);
  return totalPages;
};  