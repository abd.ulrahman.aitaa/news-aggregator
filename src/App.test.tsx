import { render, screen } from '@testing-library/react';
import App from './App';

test('renders header with title', () => {
  render(<App />);
  const headerElement = screen.getByText(/News Aggregator/i);
  expect(headerElement).toBeInTheDocument();
});