import React, { useState } from 'react';
import { categories } from '../constants/consts';

interface FilterOptionsProps {
  onCategorySelect: (categories: string[]) => void;
  onSourceSelect: (source: string) => void;
}

const FilterOptions: React.FC<FilterOptionsProps> = ({ onCategorySelect, onSourceSelect }) => {
  // const [selectedCategories, setSelectedCategories] = useState<string[]>([]);
  // const [selectedSource, setSelectedSource] = useState<string>('All');
  // Initialize state with values from localStorage or default values if not found
  const [selectedCategories, setSelectedCategories] = useState<string[]>(() => {
    const storedCategories = localStorage.getItem('selectedCategories');
    return storedCategories ? JSON.parse(storedCategories) : [];
  });

  const [selectedSource, setSelectedSource] = useState<string>(() => {
    const storedSource = localStorage.getItem('selectedSource');
    return storedSource || 'All';
  });

  const handleCategoryClick = (category: string) => {
    const newSelectedCategories = selectedCategories.includes(category)
      ? selectedCategories.filter((c) => c !== category) // Remove category if already selected
      : [...selectedCategories, category]; // Add category if not selected

    setSelectedCategories(newSelectedCategories);
    onCategorySelect(newSelectedCategories);
  };

  const handleSourceChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    const source = event.target.value;
    setSelectedSource(source);
    onSourceSelect(source); // Pass the selected source to the callback
  };

  return (
    <div className="filter-options">
      <div></div>
      <div className="category-filter">
        {categories.map((category, index) => (
          <button
            key={index}
            className={selectedCategories.includes(category.toLowerCase()) ? 'selected' : ''}
            onClick={() => handleCategoryClick(category.toLowerCase())}>
            {category}
          </button>
        ))}
      </div>
      <div className="source-filter">
        <select value={selectedSource} onChange={handleSourceChange}>
          <option value="All">All Sources</option>
          <option value="NewsAPI.org">NewsAPI.org</option>
          <option value="The Guardian">The Guardian</option>
          <option value="The New York Times">The New York Times</option>
        </select>
      </div>
    </div>
  );
};

export default FilterOptions;
