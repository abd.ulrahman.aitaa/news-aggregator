import React from 'react';
import { formatStringDate } from '../utils/helpers';
import { Article } from '../models/Article';

interface ArticleCardProps {
  article: Article;
}

//
const ArticleCard: React.FC<ArticleCardProps> = ({ article }) => {
  const formattedDate = formatStringDate(article.publishedAt);

  return (
    <div className="article-card">
      <div>
        <h2>{article.title}</h2>
        <p>{article.description}</p>
      </div>
      <p><span className="author-label">Source:</span> {article.source} - <span className="date-label">{formattedDate}</span></p> 
    </div>
  );
};

export default ArticleCard;
