import React, { useState } from 'react';
const FloatingButton: React.FC = () => {
  const [visible, setVisible] = useState(false);

  const handleScroll = () => {
    const currentScrollPos = window.pageYOffset;
    setVisible(currentScrollPos > 100); // Show button when scrolled down by 100px
  };

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth' // Smooth scroll to top
    });
  };

  React.useEffect(() => {
    window.addEventListener('scroll', handleScroll);
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);

  return (
    <button
      className={`floating-button ${visible ? 'visible' : 'hidden'}`}
      onClick={scrollToTop}>
      Top
    </button>
  );
};

export default FloatingButton;
