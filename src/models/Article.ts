export interface Article {
    id: number;
    title: string;
    description: string;
    author: string;
    publishedAt: string;
    source: string;
}