import React, { useEffect, useState } from 'react';

import Header from './components/Header';
import SearchBar from './components/SearchBar';
import FilterOptions from './components/FilterOptions';
import ArticleList from './components/ArticleList';
import Loader from './components/Loader';
import { fetchNewsFromNewYorkTimesAPI, fetchNewsFromNewsAPI, fetchNewsFromTheGuardianAPI } from './utils/api';
import FloatingButton from './components/FloatingButton';
import { Article } from './models/Article';
import { calculateTotalPages } from './utils/helpers';
import Pagination from './components/Pagination';
import { PAGE_SIZE } from './constants/consts';

const App: React.FC = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [articles, setArticles] = useState<Article[]>([]);
  const [query, setQuery] = useState<string>('');
  const [selectedCategories, setSelectedCategories] = useState<string[]>([]);
  const [selectedSource, setSelectedSource] = useState<string | null>('All');
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [totalPages, setTotalPages] = useState<number>(0);

    // Read filters from local storage when the component mounts
    useEffect(() => {
      const storedCategories = localStorage.getItem('selectedCategories');
      const storedSource = localStorage.getItem('selectedSource');
  
      if (storedCategories) {
        setSelectedCategories(JSON.parse(storedCategories));
      }
      if (storedSource) {
        setSelectedSource(storedSource);
      }
    }, []); // Empty dependency array ensures this effect runs only once

  // Define function to fetch articles
  const fetchArticles = async (query: string, page: number) => {
    try {
      // Save filters to local storage whenever they change
      localStorage.setItem('selectedCategories', JSON.stringify(selectedCategories));
      localStorage.setItem('selectedSource', selectedSource || ''); // Handle null case

      setLoading(true);

      // Check the selected source and fetch articles accordingly
      if (selectedSource === 'NewsAPI.org') {
        const { newsAPIArticles, newsAPITotal } = await fetchNewsFromNewsAPI(query, page, 'publishedAt');
        setArticles(newsAPIArticles);
        setTotalPages(calculateTotalPages(newsAPITotal, PAGE_SIZE));
      } else if (selectedSource === 'The Guardian') {
        const { guardianAPIArticles, guardianAPITotal } = await fetchNewsFromTheGuardianAPI(query, page, 'newest', 'trailText', selectedCategories[0]);
        setArticles(guardianAPIArticles);
        setTotalPages(calculateTotalPages(guardianAPITotal, PAGE_SIZE));
      } else if (selectedSource === 'The New York Times') {
        const { newYorkTimesAPIArticles, newYorkTimesAPITotal } = await fetchNewsFromNewYorkTimesAPI(query, page, 'newest', selectedCategories);
        setArticles(newYorkTimesAPIArticles);
        setTotalPages(calculateTotalPages(newYorkTimesAPITotal, PAGE_SIZE));
      } else {
        // If 'All' or no source is selected, fetch articles from all sources
        const newsAPIResponse = await fetchNewsFromNewsAPI(query, page, 'publishedAt');
        const guardianAPIResponse = await fetchNewsFromTheGuardianAPI(query, page, 'newest', 'trailText', selectedCategories[0]);
        const newYorkTimesAPIResponse = await fetchNewsFromNewYorkTimesAPI(query, page, 'newest', selectedCategories);

        const totalArticles = newsAPIResponse.newsAPITotal + guardianAPIResponse.guardianAPITotal + newYorkTimesAPIResponse.newYorkTimesAPITotal;

        const newsArticles = [
          ...newsAPIResponse.newsAPIArticles,
          ...guardianAPIResponse.guardianAPIArticles,
          ...newYorkTimesAPIResponse.newYorkTimesAPIArticles,
        ];

        setArticles(newsArticles);
        setTotalPages(calculateTotalPages(totalArticles, PAGE_SIZE));
      }

      setLoading(false);
    } catch (error) {
      console.error('Error fetching articles:', error);
      setLoading(false);
    }
  };

  // Function to handle search
  const handleSearch = (searchQuery: string) => {
    setQuery(searchQuery); // Update the query state when a new search is performed
    setCurrentPage(1); // Reset current page to 1 when performing a new search
    fetchArticles(searchQuery, 1); // Fetch articles for the first page
  };

  const handleCategorySelect = (categories: string[]) => {
    setSelectedCategories(categories);
  };

  const handleSourceSelect = (source: string) => {
    setSelectedSource(source);
  };

  const handlePageChange = (page: number) => {
    setCurrentPage(page);
    fetchArticles(query, page);
  };

  return (
    <div className="app">
      <Header title="News Aggregator" />
      <main>
        <SearchBar onSearch={handleSearch} />
        <FilterOptions onCategorySelect={handleCategorySelect} onSourceSelect={handleSourceSelect} />
        {loading ? <Loader /> : <ArticleList articles={articles} />}
        {articles.length > 0 && !loading && 
          <Pagination currentPage={currentPage} totalPages={totalPages} onPageChange={handlePageChange} />
        }
        <FloatingButton />
      </main>
    </div>
  );
};

export default App;
