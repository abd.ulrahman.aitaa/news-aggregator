// Define constants for NewsAPI
export const NEWSAPI_KEY = process.env.REACT_APP_NEWSAPI_KEY;
export const NEWSAPI_BASE_URL = process.env.REACT_APP_NEWSAPI_BASE_URL;
export const NewsAPI_EndPoint = '/everything';
// export const NewsAPI_EndPoint = '/top-headlines';

// Define constants for The Guardian API
export const THEGUARDIANAPI_KEY = process.env.REACT_APP_THEGUARDIANAPI_KEY;
export const THEGUARDIANAPI_BASE_URL = process.env.REACT_APP_THEGUARDIANAPI_BASE_URL;
export const THEGUARDIANAPI_EndPoint = '/search';

// Define constants for New York Times API
export const NEWYORKTIMESAPI_KEY = process.env.REACT_APP_NEWYORKTIMESAPI_KEY;
export const NEWYORKTIMESAPI_BASE_URL = process.env.REACT_APP_NEWYORKTIMESAPI_BASE_URL;
export const NEWYORKTIMESAPI_EndPoint = '/articlesearch.json';

export const categories = [
  'Business',
  'Entertainment',
  'General',
  'Health',
  'Science',
  'Sports',
  'Technology'
];

export const PAGE_SIZE = 10;