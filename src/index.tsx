import { createRoot } from 'react-dom/client';
import App from './App';
import './styles/main.scss';

// Get the root container element
const rootContainer = document.getElementById('root') as HTMLElement;

if (rootContainer) {
  // Create the root using the container element
  const root = createRoot(rootContainer);

  // Render your app using the new API
  root.render(<App />);
} else {
  console.error('Root container element not found.');
}
