# News Aggregator

Welcome to the News Aggregator project! This is a web application that aggregates news articles from various sources and displays them in a clean, easy-to-read format.

## Table of Contents
- [Overview](#overview)
- [Features](#features)
- [Technologies Used](#technologies-used)
- [Getting Started](#getting-started)
- [Usage](#usage)
- [Running with Docker](#running-with-docker)
- [Contributing](#contributing)
- [License](#license)

## Overview
This project is a news aggregator website built with React.js. It allows users to search for articles by keyword and filter the results by date, category, and source. Users can also customize their news feed by selecting preferred sources, categories, and authors. The website is optimized for viewing on mobile devices.

## Features
- Article search and filtering
- Personalized news feed customization
- Mobile-responsive design

## Technologies Used
- React.js
- TypeScript
- SCSS (Sass)
- Docker

## Getting Started
To get started with the project, follow these steps:

1. Clone the repository:
```
git clone <repository-url>
```
2. Navigate to the project directory:
```
cd news-aggregator
```

3. Install dependencies:
```
npm install
```
or
```
yarn install
```
## Usage
1. Run the development server:
```
npm start
```
or
```
yarn start
```
2. Open your web browser and navigate to `http://localhost:3000` to view the application.

## Running with Docker
To run the project within a Docker container, follow these steps:

1. Build the Docker image:
```
docker build -t news-aggregator .
```
2. Run the Docker container:
```
docker run -d -p 8080:80 news-aggregator
```
3. Open your web browser and navigate to `http://localhost:8080` to view the application running inside the Docker container.

## Contributing
Contributions are welcome! If you'd like to contribute to the project, please follow these steps:
1. Fork the repository.
2. Create a new branch.
3. Make your changes.
4. Test your changes.
5. Commit your changes and push to your fork.
6. Create a pull request.

## License
This project is licensed under the [MIT License](LICENSE).




## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.
